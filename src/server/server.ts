import {Server as HttpServer} from '@dkx/http-server';
import {mountMiddleware} from '@dkx/http-middleware-mount';
import {MiddlewaresLoader} from './middlewares-loader';
import {Logger, loggingMiddleware} from '../logging';
import {Config} from '../config';


export class Server
{


	private app: HttpServer;


	constructor(
		private logger: Logger,
		private middlewaresLoader: MiddlewaresLoader,
	) {
		this.app = new HttpServer;
	}
	
	
	public run(port: number, fn?: () => void): void
	{
		this.app.run(port, fn);
	}


	public configure(config: Config): void
	{
		for (let use of config.use) {
			this.app.use(this.middlewaresLoader.createMiddleware(use));
		}

		this.app.use(loggingMiddleware(this.logger));

		for (let target of config.targets) {
			this.app.use(mountMiddleware(target.path, new HttpServer(target.use.map((use) => {
				return this.middlewaresLoader.createMiddleware(use);
			}))));
		}
	}


	public close(fn?: () => void): void
	{
		this.app.close(fn);
	}
	
}
