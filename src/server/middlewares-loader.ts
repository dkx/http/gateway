import {Middleware} from '@dkx/http-server';
import {TargetMiddlewareConfig} from '../config/config';


export type MiddlewareFactory = (options: any) => Middleware;


export class MiddlewaresLoader
{


	private middlewares: {[name: string]: MiddlewareFactory} = {};


	public createMiddleware(use: TargetMiddlewareConfig): Middleware
	{
		const factory = this.loadMiddleware(use.middleware.importPath, use.middleware.factoryName);
		return factory(use.options);
	}


	private loadMiddleware(path: string, factory: string = null): MiddlewareFactory
	{
		const key = path + (factory === null ? '' : '#' + factory);

		if (typeof this.middlewares[key] === 'undefined') {
			const exportData = require(path);
			this.middlewares[key] = factory === null ? exportData : exportData[factory];
		}

		return this.middlewares[key];
	}

}
