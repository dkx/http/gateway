import {FileReader} from './file-reader';
import * as fs from 'fs';


export class NativeFileReader implements FileReader
{


	load(file: string): string
	{
		return fs.readFileSync(file, {encoding: 'utf8'});
	}

}
