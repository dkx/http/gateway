#!/usr/bin/env node


import * as yargs from 'yargs';
import * as path from 'path';
import {NativeFileReader} from '../file-reader';
import {createConfigFromFile} from '../config';
import {ConsoleLogger} from '../logging';
import {MiddlewaresLoader, Server} from '../server';


const DEFAULT_PORT: number = 8080;


const argv = yargs
	.usage('Usage $0 [options]')
	.option('config', {alias: 'c', default: path.join(process.cwd(), 'api.yml')})
	.option('port', {alias: 'p', default: DEFAULT_PORT})
	.help()
	.version(require('../../package.json').version).describe('v', 'Show installed version')
	.alias('h', 'help')
	.alias('v', 'version')
	.strict()
	.argv
;


const configFilePath: string = path.isAbsolute(argv.config) ?
	argv.config :
	path.resolve(argv.config)
;


const fileReader = new NativeFileReader;
const config = createConfigFromFile(path.dirname(configFilePath), fileReader, configFilePath, process.env);
const logger = new ConsoleLogger;
const middlewaresLoader = new MiddlewaresLoader;
const app = new Server(logger, middlewaresLoader);


app.configure(config);

app.run(argv.port, () => {
	console.log(`Listening on port ${argv.port}`);
});
