import {assert} from 'chai';
import {FileReader} from '../file-reader';
import {Config, MiddlewareConfig, MiddlewaresList, TargetConfig} from './config';
import * as yaml from 'js-yaml';
import * as path from 'path';



export function createConfigFromFile(root: string, fileReader: FileReader, file: string, envList: any = {}): Config
{
	return createConfigFromString(
		root,
		fileReader.load(file),
		envList,
	);
}


export function createConfigFromString(root: string, data: string, envList: any = {}): Config
{
	return createConfigFromYaml(
		root,
		yaml.safeLoad(data),
		envList,
	);
}


export function createConfigFromYaml(root: string, data: any, envList: any = {}): Config
{
	if (typeof data === 'undefined') {
		data = {};
	}

	const config: Config = {
		targets: [],
		use: [],
		middlewares: {
			proxy: {
				configName: 'proxy',
				importPath: '@dkx/http-middleware-proxy',
				factoryName: 'proxyMiddleware',
			},
		},
	};

	if (typeof data.middlewares !== 'undefined') {
		assert.isObject(data.middlewares, 'Config.middlewares');

		for (let name in data.middlewares) {
			if (data.middlewares.hasOwnProperty(name)) {
				config.middlewares[name] = processMiddleware(root, name, data.middlewares[name]);
			}
		}
	}

	if (typeof data.targets !== 'undefined') {
		assert.isArray(data.targets, 'Config.targets');

		for (let i = 0; i < data.targets.length; i++) {
			config.targets.push(processTarget(config.middlewares, i, data.targets[i], envList));
		}
	}

	if (typeof data.use !== 'undefined') {
		processUse(config.middlewares, envList, data.use, 'Config', (middleware, options) => {
			config.use.push({
				middleware,
				options,
			});
		});
	}

	return config;
}


function processUse(middlewares: MiddlewaresList, envList: any, use: any, errorPrefix: string, attach: (middleware: MiddlewareConfig, options: any) => void): void
{
	assert.isArray(use, `${errorPrefix}.use`);

	for (let i = 0; i < use.length; i++) {
		const middleware = use[i];

		assert.isObject(middleware, `${errorPrefix}.use[${i}]`);
		assert.isString(middleware.middleware, `${errorPrefix}.use[${i}].middleware`);

		const name = substituteVariables(envList, middleware.middleware);

		if (typeof middlewares[name] === 'undefined') {
			throw new Error(`Using unknown middleware "${name}"`);
		}

		const options = typeof middleware.options === 'undefined' ? {} : middleware.options;

		for (let optionName in options) {
			if (options.hasOwnProperty(optionName)) {
				options[optionName] = substituteVariables(envList, options[optionName]);
			}
		}

		attach(
			middlewares[name],
			options,
		);
	}
}


function substituteVariables(envList: any, s: any): any
{
	if (Array.isArray(s)) {
		return s.map((item) => substituteVariables(envList, item));
	}

	if (Object.prototype.toString.call(s) === '[object Object]') {
		for (let item in s) {
			if (s.hasOwnProperty(item)) {
				s[item] = substituteVariables(envList, s[item]);
			}
		}

		return s;
	}

	if (typeof s === 'string') {
		return s.replace(/\${env:([a-zA-Z][a-zA-Z_]*)}/g, (group, env) => {
			assert.isDefined(envList[env], `Config: environment variable ${env} does not exists`);
			return envList[env];
		});
	}

	return s;
}


function processTarget(middlewares: MiddlewaresList, index: number, data: any, envList: any): TargetConfig
{
	assert.isObject(data, `Config.target[${index}]`);
	assert.isString(data.path, `Config.target[${index}].path`);

	const target: TargetConfig = {
		path: substituteVariables(envList, data.path),
		use: [],
	};

	if (typeof data.proxy !== 'undefined') {
		if (typeof data.use === 'undefined') {
			data.use = [];
		}

		data.use.push({
			middleware: 'proxy',
			options: data.proxy,
		});
	}

	if (typeof data.use !== 'undefined') {
		processUse(middlewares, envList, data.use, `Config.target[${index}]`, (middleware, options) => {
			target.use.push({
				middleware,
				options,
			});
		});
	}

	return target;
}


function processMiddleware(root, name: string, middleware: string): MiddlewareConfig
{
	const parts = middleware.split('#');

	// transform relative middleware path to absolute
	if (parts[0].match(/^\.\//)) {
		parts[0] = path.join(root, parts[0]);
	}

	const result: MiddlewareConfig = {
		configName: name,
		importPath: parts[0],
	};

	if (parts.length > 1) {
		result.factoryName = parts[1];
	}

	return result;
}
