export {Config, TargetConfig} from './config';
export {createConfigFromFile, createConfigFromString, createConfigFromYaml} from './config-factory';
