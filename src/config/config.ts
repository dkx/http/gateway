export declare interface MiddlewareConfig
{
	configName: string,
	importPath: string,
	factoryName?: string,
}


export declare interface TargetMiddlewareConfig
{
	middleware: MiddlewareConfig,
	options: any,
}


export declare interface TargetConfig
{
	path: string,
	use: Array<TargetMiddlewareConfig>,
}


export declare interface MiddlewaresList
{
	[name: string]: MiddlewareConfig,
}


export declare interface Config
{
	middlewares: MiddlewaresList,
	targets: Array<TargetConfig>,
	use: Array<TargetMiddlewareConfig>,
}
