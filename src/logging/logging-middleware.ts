import {Middleware, NextMiddlewareFunction, Request, Response, RequestState} from '@dkx/http-server';
import {Logger} from './logger';
import {performance} from 'perf_hooks';


export function loggingMiddleware(logger: Logger): Middleware
{
	return async (req: Request, res: Response, next: NextMiddlewareFunction, state: RequestState): Promise<Response> => {
		const start = performance.now();
		res = await next(res);
		const end = performance.now();
		const elapsed = Math.round((end - start) * 1000) / 1000;

		if (typeof state.mount !== 'undefined') {
			const url = state.mount.innerState.proxy.host + state.mount.url;

			const time = typeof state.targetResponseElapsed === 'undefined' ?
				elapsed :
				(Math.round((state.targetResponseElapsed) * 1000) / 1000) + '/' + elapsed
			;

			logger.log(`${req.method} ${req.url} ${res.statusCode} => ${req.method} ${url} ${time}ms`);

		} else {
			logger.log(`${req.method} ${req.url} 404 => No endpoint ${elapsed}ms`);
		}

		return res;
	};
}
