import '../bootstrap';

import {expect} from 'chai';
import {createConfigFromYaml} from '../../../lib/config';


describe('#Config/ConfigFactory', () => {

	describe('createConfigFromYaml()', () => {

		it('should throw an error when targets is not an array', () => {
			expect(() => {
				createConfigFromYaml('/', {
					targets: false,
				});
			}).to.throw(Error, 'Config.targets: expected false to be an array');
		});

		it('should throw an error when target is not an object', () => {
			expect(() => {
				createConfigFromYaml('/', {
					targets: [false],
				});
			}).to.throw(Error, 'Config.target[0]: expected false to be an object');
		});

		it('should throw an error when target path is not a string', () => {
			expect(() => {
				createConfigFromYaml('/', {
					targets: [
						{},
					],
				});
			}).to.throw(Error, 'Config.target[0].path: expected undefined to be a string');
		});

		it('should parse middlewares', () => {
			const config = createConfigFromYaml('/', {
				middlewares: {
					test: 'test-middleware#testMiddleware',
				},
			});

			expect(config).to.be.eql({
				targets: [],
				use: [],
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: 'test-middleware',
						factoryName: 'testMiddleware',
					},
				},
			});
		});

		it('should parse middleware without export name', () => {
			const config = createConfigFromYaml('/', {
				middlewares: {
					test: 'test-middleware',
				},
			});

			expect(config).to.be.eql({
				targets: [],
				use: [],
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: 'test-middleware',
					},
				},
			});
		});

		it('should parse relative middlewares', () => {
			const config = createConfigFromYaml('/root', {
				middlewares: {
					test: './test-middleware#testMiddleware',
				},
			});

			expect(config).to.be.eql({
				targets: [],
				use: [],
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: '/root/test-middleware',
						factoryName: 'testMiddleware',
					},
				},
			});
		});

		it('should throw an error use is not an array', () => {
			expect(() => {
				createConfigFromYaml('/root', {
					use: {},
				});
			}).to.throw(Error, 'Config.use: expected {} to be an array');
		});

		it('should throw an error use middleware is not an object', () => {
			expect(() => {
				createConfigFromYaml('/root', {
					use: [
						[],
					],
				});
			}).to.throw(Error, 'Config.use[0]: expected [] to be an object');
		});

		it('should throw an error middleware name in use is not a string', () => {
			expect(() => {
				createConfigFromYaml('/root', {
					use: [
						{},
					],
				});
			}).to.throw(Error, 'Config.use[0].middleware: expected undefined to be a string');
		});

		it('should throw an error if global middleware is not registered', () => {
			expect(() => {
				createConfigFromYaml('/root', {
					use: [
						{
							middleware: 'test',
						},
					],
				});
			}).to.throw(Error, 'Using unknown middleware "test"');
		});

		it('should use global middleware', () => {
			const config = createConfigFromYaml('/root', {
				middlewares: {
					test: 'test-middleware#testMiddleware',
				},
				use: [
					{
						middleware: 'test',
						options: {
							message: 'hello world',
						},
					},
				],
				targets: [
					{
						path: '/root',
					},
				],
			});

			expect(config).to.be.eql({
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: 'test-middleware',
						factoryName: 'testMiddleware',
					},
				},
				use: [
					{
						middleware: {
							configName: 'test',
							importPath: 'test-middleware',
							factoryName: 'testMiddleware',
						},
						options: {
							message: 'hello world',
						},
					},
				],
				targets: [
					{
						path: '/root',
						use: [],
					},
				],
			});
		});

		it('should add global middleware without options', () => {
			const config = createConfigFromYaml('/root', {
				middlewares: {
					test: 'test-middleware#testMiddleware',
				},
				use: [
					{
						middleware: 'test',
					},
				],
				targets: [
					{
						path: '/root',
					},
				],
			});

			expect(config).to.be.eql({
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: 'test-middleware',
						factoryName: 'testMiddleware',
					},
				},
				use: [
					{
						middleware: {
							configName: 'test',
							importPath: 'test-middleware',
							factoryName: 'testMiddleware',
						},
						options: {},
					},
				],
				targets: [
					{
						path: '/root',
						use: [],
					},
				],
			});
		});

		it('should throw an error if specific use middleware is not an object', () => {
			expect(() => {
				createConfigFromYaml('/root', {
					targets: [
						{
							path: '/root',
							use: [
								[],
							],
						},
					],
				});
			}).to.throw(Error, 'Config.target[0].use[0]: expected [] to be an object');
		});

		it('should add target specific middleware', () => {
			const config = createConfigFromYaml('/root', {
				middlewares: {
					test: 'test-middleware#testMiddleware',
				},
				targets: [
					{
						path: '/root',
						use: [
							{
								middleware: 'test',
								options: {
									message: 'hello world',
								},
							},
						],
					},
					{
						path: '/other',
					},
				],
			});

			expect(config).to.be.eql({
				use: [],
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
					test: {
						configName: 'test',
						importPath: 'test-middleware',
						factoryName: 'testMiddleware',
					},
				},
				targets: [
					{
						path: '/root',
						use: [
							{
								middleware: {
									configName: 'test',
									importPath: 'test-middleware',
									factoryName: 'testMiddleware',
								},
								options: {
									message: 'hello world',
								},
							},
						],
					},
					{
						path: '/other',
						use: [],
					},
				],
			});
		});

		it('should substitute use', () => {
			const config = createConfigFromYaml('/', {
				use: [
					{
						middleware: '${env:middleware}',
						options: {
							message: '${env:message}',
							array: [
								'${env:array}',
							],
							object: {
								item: '${env:object}',
							},
						},
					},
				],
			}, {
				middleware: 'proxy',
				message: 'hello world',
				array: 'is array',
				object: 'is object',
			});

			expect(config).to.be.eql({
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
				},
				use: [
					{
						middleware: {
							configName: 'proxy',
							importPath: '@dkx/http-middleware-proxy',
							factoryName: 'proxyMiddleware',
						},
						options: {
							message: 'hello world',
							array: [
								'is array',
							],
							object: {
								item: 'is object',
							},
						},
					},
				],
				targets: [],
			});
		});

		it('should add use for proxy from shortcut', () => {
			const config = createConfigFromYaml('/', {
				targets: [
					{
						path: '/users',
						proxy: {
							host: 'http://users.localhost',
						},
					},
				],
			});

			expect(config).to.be.eql({
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
				},
				use: [],
				targets: [
					{
						path: '/users',
						use: [
							{
								middleware: {
									configName: 'proxy',
									importPath: '@dkx/http-middleware-proxy',
									factoryName: 'proxyMiddleware',
								},
								options: {
									host: 'http://users.localhost',
								},
							},
						],
					},
				],
			});
		});

	});

});
