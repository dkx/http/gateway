import '../bootstrap';

import {RequestState, testMiddleware} from '@dkx/http-server';
import {expect} from 'chai';
import {loggingMiddleware} from '../../../lib/logging';
import {MockLogger} from '../../mocks';


let logger: MockLogger;


describe('#Logging/loggingMiddleware', () => {

	beforeEach(() => {
		logger = new MockLogger;
	});

	it('should log unknown endpoint', async () => {
		await testMiddleware(loggingMiddleware(logger), {
			url: '/',
		});

		expect(logger.messages[0]).to.startWith('GET / 404 => No endpoint');
	});

	it('should log target endpoint', async () => {
		const state: RequestState = {
			mount: {
				path: '/users',
				url: '/v1',
				innerState: {
					proxy: {
						host: 'http://localhost:8080',
					},
				},
			},
		};

		await testMiddleware(loggingMiddleware(logger), {
			state,
			url: '/users/v1',
		});

		expect(logger.messages[0]).to.startWith('GET /users/v1 200 => GET http://localhost:8080/v1');
	});

});
