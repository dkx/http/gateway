import '../bootstrap';

import {Server as HttpServer} from '@dkx/http-server';
import {responseMiddleware} from '@dkx/http-middleware-response';
import {expect} from 'chai';
import {Server, MiddlewaresLoader} from '../../../lib/server';
import {MockLogger} from '../../mocks';
import fetch from 'node-fetch';


let app: Server;
let target: HttpServer;


describe('#Server/Server', () => {

	beforeEach(() => {
		app = new Server(new MockLogger, new MiddlewaresLoader);
		target = new HttpServer;
	});

	describe('run()', () => {

		beforeEach((done) => {
			app.run(8080, () => {
				target.run(8081, done);
			});
		});

		afterEach((done) => {
			app.close(() => {
				target.close(done);
			});
		});

		it('should run target', async () => {
			target.use(responseMiddleware({
				statusMessage: 'a',
			}));

			app.configure({
				middlewares: {
					proxy: {
						configName: 'proxy',
						importPath: '@dkx/http-middleware-proxy',
						factoryName: 'proxyMiddleware',
					},
				},
				use: [],
				targets: [
					{
						path: '/a',
						use: [
							{
								middleware: {
									configName: 'proxy',
									importPath: '@dkx/http-middleware-proxy',
									factoryName: 'proxyMiddleware',
								},
								options: {
									host: 'http://localhost:8081',
								},
							},
						],
					},
				],
			});

			const res = await fetch('http://localhost:8080/a/test');

			expect(res.statusText).to.be.equal('a');
		});

	});

});
