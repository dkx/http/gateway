import '../bootstrap';

import {expect} from 'chai';
import {NativeFileReader} from '../../../lib/file-reader';
import * as path from 'path';


let fileReader: NativeFileReader;


describe('#FileReader/NativeFileReader', () => {

	beforeEach(() => {
		fileReader = new NativeFileReader;
	});

	describe('load()', () => {

		it('should load file', () => {
			const filePath = path.join(__dirname, '..', '..', 'data', 'file-reader', 'file.txt');
			const content = fileReader.load(filePath);

			expect(content).to.be.equal('Hello world\n');
		});

	});

});
