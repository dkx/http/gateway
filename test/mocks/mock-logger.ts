import {Logger} from '../../lib/logging';


export class MockLogger implements Logger
{


	public readonly messages: Array<string> = [];


	public log(message: string): void
	{
		this.messages.push(message);
	}

}
