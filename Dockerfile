FROM node:10.8.0-alpine as build
LABEL maintainer="David Kudera <kudera.d@gmail.com>"


ENV NODE_ENV=development
WORKDIR /usr/src/app
COPY . /usr/src/app

RUN npm install && \
	npm run build


FROM node:10.8.0-alpine
LABEL maintainer="David Kudera <kudera.d@gmail.com>"


ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ./bin/start.sh /start
COPY ./bin/entrypoint.sh /entrypoint
COPY --from=build /usr/src/app/lib/ ./lib
COPY --from=build /usr/src/app/package.json .
COPY --from=build /usr/src/app/package-lock.json .

RUN npm install && \
	mkdir data && \
	chmod +x lib/bin/main.js && \
	chmod +x /start && \
	chmod +x /entrypoint

EXPOSE 8080
VOLUME /config
VOLUME /middleware

ENTRYPOINT ["/entrypoint"]
CMD ["/start"]
