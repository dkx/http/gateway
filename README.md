# DKX/HttpGateway

Easy to use API gateway based on [@dkx/http-server](https://gitlab.com/dkx/http/server).

## Built in middlewares

* `proxy`: [@dkx/http-middleware-proxy](https://gitlab.com/dkx/http/middlewares/proxy)

## Usage

```bash
docker run -p 8080:8080 -v /path/to/my/config-directory:/config registry.gitlab.com/dkx/http/gateway
```

## Configuration

Configuration is written in YAML and is expected to be in file named `api.yml`.

```yaml
targets:

  - url: /users
    proxy:
      host: http://users:8080
```

Now all requests to `http://localhost:8080/users/*` will be redirected to `http://users/8080/*`.

For all available `proxy` options, read the [@dkx/http-middleware-proxy](https://gitlab.com/dkx/http/middlewares/proxy) 
documentation.

## Adding middlewares

To be able to use another middlewares, you'll have to register them in your config.

```yaml
middlewares:
  cors: @dkx/http-middleware-cors#corsMiddleware

use:
  - middleware: cors
    options:
      domains: [*]
      
targets: [...]
```

Here we said to use the [@dkx/http-middleware-cors](https://gitlab.com/dkx/http/middlewares/cors) globally.

It is also possible to use middleware per target:

```yaml
middlewares:
  cors: @dkx/http-middleware-cors#corsMiddleware
  
targets:

  - url: /users
    proxy:
      host: http://users:8080
    use:
      - middleware: cors
        options:
          domains: [*]
```

In fact the `proxy` key in `targets` is just a shortcut for `targets.use.proxy`.

**The order of middlewares in `use` is important!!!**

The part of middleware after `#` is name of exported middleware factory function. Export name can be omitted. 

## Custom middlewares

If you have your own middleware which you would like to use, you can take advantage of prepared `/middleware` docker 
volume. Just mount it in your app, place the middlewares there and import them in the yaml config:

```yaml
middlewares:
  customMiddleware: /middleware/custom-middleware
```

The `/middleware/custom-middleware` is passed into nodejs `require` function.

## Installing middlewares from NPM

The best option here is to create custom `Dockerfile` and install the middleware.

```dockerfile
FROM registry.gitlab.com/dkx/http/gateway
RUN npm install @dkx/http-middleware-cors
```

and specify it like you normally would:

```yaml
middlewares:
  cors: @dkx/http-middleware-cors#corsMiddleware
```

## Environment variables substitution

```yaml
middlewares:
  cors: @dkx/http-middleware-cors#corsMiddleware

use:
  - middleware: cors
    options:
      domains: [${env:DOMAIN_NAME}]
      
targets: [...]
```

The `${env:DOMAIN_NAME}` will be replaced with value of `DOMAIN_NAME` environment variable.
